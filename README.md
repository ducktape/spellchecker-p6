# spellchecker-p6

Peter Norvigs spellchecker done in Perl 6

The original source can be found [here](https://norvig.com/spell-correct.html).
This is mostly intended to be used as a way to learn Perl 6 and give something
to hack on to make other cool things. I'd eventually like to tie it into the
perl6 version of the Kilo editor. 
